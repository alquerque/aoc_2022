import sys

def find_start_marker(marker_size, stream):
    for k in range(len(stream)):
        if len(set(stream[k:k+marker_size])) == marker_size:
            return k + marker_size


stream_len4 = []
stream_len14 = []
with open(sys.argv[1], 'r') as fin:
    streams = fin.read().split('\n')
    
    for stream in streams:
        if not stream:
            continue
        stream_len4.append(find_start_marker(4, stream))
        stream_len14.append(find_start_marker(4, stream))
        

print('For a marker size of 4 the start character are at', stream_len4)
print('For a marker size of 14 the start character are at', stream_len14)

