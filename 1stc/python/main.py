import sys
import dataclasses
from typing import List
from functools import reduce

@dataclasses.dataclass
class Elf:
    name : str
    count : int
    food: List[int]

    @classmethod
    def create(cls, name, food_list):
        try:
            instance = cls(name=name, count=0, food=[int(line) for line in food_list.split('\n')])
            instance.inv()
        except Exception as e:
            print(name)
            raise e
        return instance

    def __str__(self):
        return f'Elf: {self.name} has Food: {self.food} for a total of {self.count}'

    def inv(self):
        self.count = reduce(lambda x,y: x+y, self.food, 0)
with open(sys.argv[1], 'r') as f_in:
    each_elf_lists = f_in.read().split('\n\n')
    elfs = [Elf.create(pk + 1, each_elf_list) for pk, each_elf_list in enumerate(each_elf_lists)]

    
sorted_elves = sorted(elfs, key=lambda x: x.count, reverse=True)

print("THe richest", sorted_elves[0].name, sorted_elves[0].count)
print("with food", sorted_elves[0].food)
print(sorted_elves[0])

print('Top three are')
total = 0
for i in range(3):
    print(sorted_elves[i])
    total += sorted_elves[i].count

print('Total top 3', total)