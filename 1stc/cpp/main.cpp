#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <fstream>

class Elf{
    public: 
        int m_name;
        int m_count;
        std::vector<int> m_food;
        Elf(int name, int count, std::vector<int> food) {m_name = name; m_food = m_food; count=m_count;}
        Elf(int name, const std::vector<std::string> & food) {
            m_name = name;
            m_count = 0;
            for(auto item : food){
                const int calories_per_item = std::stoi(item);
                m_count += calories_per_item;
                m_food.push_back(calories_per_item);
            }
        }
        
        std::string to_string(){
            std::stringstream formatted_stream;
            formatted_stream << "I am elf " << m_name << " and I have " << m_count <<" calories with me and this food: [";
            for (std::vector<int>::iterator item_ptr=m_food.begin(); item_ptr < m_food.end() ;item_ptr++){
                if(std::distance(item_ptr, m_food.end()) > 1){
                    formatted_stream << *item_ptr << ", ";
                } else {
                    formatted_stream << *item_ptr;
                }
            }
            formatted_stream << "]";
            return formatted_stream.str();
        }
};

void read_file_in_vector(const std::string & fname, std::vector<Elf> & elves){
    std::ifstream fstream;
    
    fstream.open(fname);

    if(fstream.is_open()){
        std::vector<std::string> food_items;
        int count=0;
        for(std::string line; std::getline(fstream, line);){
            if(line != ""){
                food_items.push_back(line);
            }else{
                count += 1;
                elves.push_back(Elf(count, food_items));
                food_items.clear();
            }
        }
    } else {
        std::cout << "Cannot open file " << fname << std::endl;
    }

}

int main(int argc, char *argv[]){
    if(argc != 2 ) {
        std::cout << "Please specify input file"<<std::endl;
        return 1;
    }

    std::cout<<"Hello people"<<std::endl;
    std::vector<Elf> elves;
    read_file_in_vector(std::string(argv[1]), elves);
    std::sort(elves.begin(), elves.end(), [] (const Elf & a, const Elf & b) { return a.m_count > b.m_count;});
    
    std::cout<< "The Elf with the most calories is ..." << std::endl;
    std::cout<<elves[0].to_string()<<std::endl;
    
    std::cout<< "the top 3 elves are" << std::endl;
    int total = 0;
    for(int i=0; i<3; i++){
        std::cout<<elves[i].to_string()<<std::endl;
        total += elves[i].m_count;
    }
    std::cout<<"For a total of "<<total << std::endl;
}