package main

import (
	"fmt"
	"os"
	"strings"
	"strconv"
	"sort"
)

type Elf struct{
	name int
	count int
	food []int
}

func (elf Elf) String() string {
	return fmt.Sprintf("Elf %v has %v calories and the following food %v", elf.name, elf.count, elf.food)
}

func createElfFromItems(name int, itemsList string) Elf {
	itemsCalories := make([]int, 0)
	count := 0
	for _, item := range(strings.Split(itemsList, "\n")){
		itemValue, _ := strconv.Atoi(item)
		itemsCalories = append(itemsCalories, itemValue)
		count += itemValue
	}
	return Elf{name, count, itemsCalories}
}


func checkReadError(e error){
	if e != nil {
		panic(e)
	}
}

func parseInputFile(data []byte) (elves []Elf) {
	items := strings.Split(string(data), "\n\n")
	
	for name, itemsList := range(items) {
		elves = append(elves, createElfFromItems(name + 1, itemsList))
	}
	return elves
}

func sortByCalories(elves []Elf){
	// Less comparison is reverted as I want to sort descending
	// A is less then b when a.count is bigger then b.count so it should go first
	sort.SliceStable(elves, func(a, b int) bool { 
		return elves[a].count > elves[b].count
	})
}

func main() {
	defer func () {
		if r:=recover(); r!= nil {
			fmt.Println("Error: ", r)
		}
	}()

	fname := ""

	if len(os.Args) == 2 {
		fname = os.Args[1]
	} else {
		panic("please specify a file path")
	}

	data, err := os.ReadFile(fname)
	checkReadError(err)

	elves := parseInputFile(data)
	sortByCalories(elves)

	fmt.Println("The Elf with the highest calories count is.....")
	fmt.Println(elves[0])

	fmt.Println("The top 3 elves are: ")
	count := 0
	for _, elf := range(elves[:3]) {
		fmt.Println(elf)
		count += elf.count
	}
	fmt.Println("For a total count of", count)
}