import sys
from functools import reduce

import matplotlib.pyplot as plt
import numpy


def sign(a, b):
    if a == b:
        return 0
    if a > b:
        return -1
    else:
        return 1


def print_wall(wall):
    smallest_y, biggest_y, smallest_x, biggest_x = stone_boundaries(wall)
    print('WALL'.center(10, '='))
    for i in range(smallest_y, biggest_y + 1):
        print(' '.join([wall[(j, i)] if (j, i) in wall else f'.' for j in
                        range(smallest_x, biggest_x + 1)]))


def char_to_number(c):
    if c == '.':
        return -10
    if c == '#':
        return -5
    if c == 'o':
        return 2
    if c == '+':
        return 1


def wall_to_array(wall):
    smallest_y, biggest_y, smallest_x, biggest_x = stone_boundaries(wall)
    lines = []
    for i in range(smallest_y, biggest_y + 1):
        lines.append(
            [char_to_number(wall[(j, i)]) if (j, i) in wall else char_to_number('.') for
             j in
             range(smallest_x, biggest_x + 1)])

    return numpy.array(lines)


def parse_wall(lines):
    wall = {(0, 0): '+'}

    for line in lines:
        vertices = list(map(lambda x: (x[0] - 500, x[1]),
                            map(lambda x: tuple(map(int, x.split(','))),
                                line.split(' -> '))))

        for k, vertex in enumerate(vertices[:-1]):
            next_vertex = vertices[k + 1]
            distance = sum([abs(xa - xb) for xa, xb in zip(vertex, next_vertex)])
            direction = list(map(lambda x: sign(x[0], x[1]), zip(vertex, next_vertex)))

            point = tuple(vertex)
            for i in range(distance + 1):
                wall[point] = '#'
                point = tuple([x + d for x, d in zip(point, direction)])
    return wall


def stone_boundaries(wall):
    smallest_x = reduce(min, map(lambda x: x[0], wall.keys()), 0)
    biggest_x = reduce(max, map(lambda x: x[0], wall.keys()), 0)
    smallest_y = reduce(min, map(lambda x: x[1], wall.keys()), 0)
    biggest_y = reduce(max, map(lambda x: x[1], wall.keys()), 0)
    return [smallest_y, biggest_y, smallest_x, biggest_x]


def drop_single_sand_grain(wall, start_point, boundaries, with_floor=False):
    smallest_y, biggest_y, smallest_x, biggest_x = boundaries
    point = tuple(start_point)
    for i in range(biggest_y + 1):
        if (point[0], point[1] + 1) in wall:
            if (point[0] - 1, point[1] + 1) in wall:
                if (point[0] + 1, point[1] + 1) in wall:
                    if point == (0, 0):
                        return False
                    wall[point] = 'o'
                    return True
                else:
                    point = point[0] + 1, point[1] + 1
            else:
                point = point[0] - 1, point[1] + 1
        else:
            point = point[0], point[1] + 1
    else:

        if with_floor:
            wall[point] = 'o'
            return True
        else:
            return False


with open(sys.argv[1], 'r') as fin:
    lines = list(filter(lambda x: x, fin.read().split('\n')))
    wall = parse_wall(lines)
    print_wall(wall)
    boundaries = stone_boundaries(wall)
    while drop_single_sand_grain(wall, (0, 0), boundaries):
        if len(sys.argv) == 3:
            print_wall(wall)
            input('Continue?')
    print('Sand is', len([k for k in wall.values() if k == 'o']))
    plt.imshow(wall_to_array(wall))
    plt.show()

    wall = parse_wall(lines)
    while drop_single_sand_grain(wall, (0, 0), boundaries, with_floor=True):
        if len(sys.argv) == 3:
            print_wall(wall)
            input('Continue?')
    print('Sand is', len([k for k in wall.values() if k == 'o']) + 1)

    plt.imshow(wall_to_array(wall))
    plt.show()
