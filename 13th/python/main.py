import json
import sys
from functools import reduce


def compare_is_right_order(item_a, item_b, indent=''):
    print(indent, 'Compare', item_a, 'vs ', item_b)

    if isinstance(item_a, int) and isinstance(item_b, int):
        if item_a < item_b:
            print(indent, '-', 'Left side is smaller so inputs are in the right order')
            return 1
        elif item_a == item_b:
            return 0
        else:
            print(indent, '-',
                  'Right side is smaller, so inputs are not in the right order')
            return -1
    elif isinstance(item_a, list) and isinstance(item_b, list):
        for k, val_a in enumerate(item_a):
            if k == len(item_b):
                print(indent, '-', 'Right side ran out of items so not right order')
                return -1
            comparison = compare_is_right_order(val_a, item_b[k], indent=indent + '  ')
            if comparison != 0:
                return comparison
        if len(item_a) == 0 and len(item_b) == 0:
            return 0
        if len(item_a) == 0 or len(item_a) < len(item_b):
            return 1
        return 0

    elif isinstance(item_a, int):
        return compare_is_right_order([item_a], item_b, indent=indent)
    elif isinstance(item_b, int):
        return compare_is_right_order(item_a, [item_b], indent=indent)


with open(sys.argv[1], 'r') as fin:
    couples = list(map(lambda x: [json.loads(k) for k in x.split('\n')],
                       filter(lambda x: x, fin.read().rstrip('\n').split('\n\n'))))

    result = []
    for i, (seq_a, seq_b) in enumerate(couples):
        comparison_result = compare_is_right_order(seq_a, seq_b)
        if comparison_result >= 0:
            result.append(i + 1)

    print(reduce(lambda x, y: x + y, result, 0))
from functools import cmp_to_key

with open(sys.argv[1], 'r') as fin:
    packets = list(map(lambda x: json.loads(x),
                       filter(lambda x: x, fin.read().split('\n'))))

    packets.append([[2]])
    packets.append([[6]])
    sorted_packets = sorted(packets, key=cmp_to_key(compare_is_right_order),
                            reverse=True)
    indexes = [i + 1 for i, packet in enumerate(sorted_packets) if
               packet in [[[2]], [[6]]]]
    print('Indexes are', indexes, 'and key is ', reduce(lambda x, y: x * y, indexes, 1))
