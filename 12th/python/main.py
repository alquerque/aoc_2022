import sys
import tqdm
from joblib import Parallel, delayed

def print_matrix(matrix):
    for line in matrix:
        print(' '.join(map(lambda x: str(x).center(2, ' '), line)))
        print()


def char_to_in(char):
    if char.isupper() and char == 'S':
        return 0
    if char.isupper() and char == 'E':
        return ord('z') - ord('a') + 2

    return ord(char) - ord('a') + 1


import math


def distance(map, point_a, point_b):
    val_a = map[point_a[0]][point_a[1]]
    val_b = map[point_b[0]][point_b[1]]

    if val_b <= val_a + 1:

        dist = abs(point_b[0] - point_a[0]) + abs(point_b[1] - point_a[1])
        return dist
    else:
        return math.inf

def parse_map(map):
    matrix = [[char_to_in(c) for c in line] for line in map if
              line]
    start_point = None
    for i, line in enumerate(matrix):
        for j, col in enumerate(line):
            if matrix[i][j] == 0:
                start_point = (i, j)
    return matrix, start_point

def dijkstra(map, start_point, till_the_end=False):
    tmp_map = [[c for c in line] for line in map]

    dist = {}
    prev = {}
    unvisited_set = set()
    start_node = tuple(start_point)
    last_node = (0, 0)

    for i, row, in enumerate(map):
        for k, cel in enumerate(row):
            if cel == 27:
                dist[(i, k)] = math.inf
                tmp_map[i][k] = 26
                last_node = (i, k)
            else:
                dist[(i, k)] = math.inf
            unvisited_set.add((i, k))
    dist[start_node] = 0

    while len(unvisited_set):
        current_node = sorted(unvisited_set, key=lambda x: dist[x])[0]
        if math.isinf(dist[current_node]):
            raise Exception('No solution')
        unvisited_set.remove(current_node)

        if distance(tmp_map, current_node, last_node) == 0 and not till_the_end:
            return dist, prev, start_node, last_node
        neighbours = list(
            filter(lambda x: distance(tmp_map, current_node, x) <= 1, unvisited_set))

        for neighbour in neighbours:
            t_dist = distance(tmp_map, current_node, neighbour) + dist[current_node]

            if t_dist < dist[neighbour]:
                dist[neighbour] = t_dist
                prev[neighbour] = current_node

    return dist, prev, start_node, last_node


def compute_path(matrix, start_point):
    dist, prev, start_node, last_node = dijkstra(matrix, start_point)
    u = last_node
    path = []
    while u in prev:
        u = prev[u]
        path.append(u)
    return path

def compute_forward_path(successive, start_point, end_point):
    u = start_point
    path = []
    while u in successive:
        path.append(u)
        u = successive[u]

        print('jo', u, start_point, end_point)
        if u[0] == end_point[0] and u[1] == end_point[1]:
            return path
    return []


def find_a(matrix):
    items = []

    start_points = []
    for i, row, in enumerate(matrix):
        for k, cel in enumerate(row):
            # IS a
            if cel == 1:
                start_points.append((i, k))

    for point in tqdm.tqdm(start_points):
        try:
            path = compute_path(matrix, point)
            items.append((point, len(path)))
        except Exception as e:
            print(point, 'Doesnt reach the end')
    return sorted(items, key=lambda x: x[1])

if __name__ == '__main__':
    matrix = []
    with open(sys.argv[1], 'r') as fin:

        matrix, start_point = parse_map(fin.read().split('\n'))
        path = compute_path(matrix, start_point)
        print(len(path))

        print('-' * 20)
        short_paths = find_a(matrix)
        for start_point, path_length in short_paths[:5]:
            print(f'Starting from {start_point} you do ', path_length, 'steps')
