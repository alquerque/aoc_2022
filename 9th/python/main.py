import sys

import numpy as np


class Board:
    def __init__(self, length=2, max_length=10) -> None:
        self.rope = [[0, 0] for i in range(length)]

        self.length = length
        self.max_length = max_length
        self.visited = {(0, 0)}

    def should_t_move(self, head, tail):
        (hx, hy), (tx, ty) = head, tail

        if hx == tx and hy == ty:
            return False
        if abs(hx - tx) <= 1 and abs(hy - ty) <= 1:
            return False
        else:
            return True

    def __str__(self):
        lines = []
        for i in range(2 * self.max_length):
            line = [' ' for _ in range(2 * self.max_length)]
            lines.append(line)

        lines[self.max_length][self.max_length] = 'S'
        lines[self.rope[-1][0] + self.max_length][
            self.rope[-1][1] + self.max_length] = 'T'

        for k in range(1, self.length - 1):
            value = str(k)
            lines[self.rope[k][0] + self.max_length][
                self.rope[k][1] + self.max_length] = value
        lines[self.rope[0][0] + self.max_length][
            self.rope[0][1] + self.max_length] = 'H'
        return "\n".join(list(map(lambda x: ''.join(x), lines))[::-1])

    def step_element(self, head, tail, d):
        h_pos, t_pos = self.rope[head], self.rope[tail]
        if t_pos[0] == h_pos[0] and t_pos[1] == h_pos[1]:
            return

        next_t = None
        diff = (h_pos[0] - t_pos[0]), (h_pos[1] - t_pos[1])

        if abs(diff[0]) * abs(diff[1]) in [2, 4]:
            next_t = [t_pos[0] + np.sign(diff[0]), t_pos[1] + np.sign(diff[1])]
        elif abs(diff[0]) * abs(diff[1]) == 0:
            next_t = [t_pos[0] + np.sign(diff[0]), t_pos[1] + np.sign(diff[1])]

        if next_t is None:
            return

        if next_t[0] == h_pos[0] and next_t[1] == h_pos[1]:
            return
        else:
            self.rope[tail] = list(next_t)

    def step_head(self, d):
        h_pos = self.rope[0]
        if d == 'R':
            h_pos[0], h_pos[1] = h_pos[0], h_pos[1] + 1
        elif d == 'L':
            h_pos[0], h_pos[1] = h_pos[0], h_pos[1] - 1
        elif d == 'U':
            h_pos[0], h_pos[1] = h_pos[0] + 1, h_pos[1]
        elif d == 'D':
            h_pos[0], h_pos[1] = h_pos[0] - 1, h_pos[1]

    def step(self, d):
        self.step_head(d)
        for i in range(0, self.length - 1):
            self.step_element(i, i + 1, d)

        self.visited.add(tuple(self.rope[-1]))

    def move(self, d, count):

        for c in range(count):
            self.step(d)


with open(sys.argv[1], 'r') as fin:
    instructions = list(filter(lambda x: x, fin.read().split('\n')))
    # board = Board(length=2)
    # for instruction in instructions[:2]:
    #     d, c = instruction.split(' ')
    #
    #     board.move(d, int(c))
    #     if len(sys.argv) > 2:
    #         print(instruction.center(50, '-'))
    #         print(board)
    # print("FINAL".center(50, '-'))
    # print(len(board.visited))
    #
    board2 = Board(length=10, max_length=20)
    for instruction in instructions:
        d, c = instruction.split(' ')
        if len(sys.argv) > 2:
            print(instruction.center(50, '-'))
            print(board2)
        board2.move(d, int(c))

    print("FINAL".center(50, '-'))
    print(len(board2.visited))
