import sys
from functools import reduce
class Leaf:
    def __init__(self, parent, path, type='Dir', size=0):
        self.parent = parent
        self.path = path
        self.type = type
        self.size = size
        self.children = {}

    def get_size(self):
        if self.type == 'Dir':
            count = 0
            for child in self.children.values():
                count += child.get_size()
            return count
        else:
            return self.size

    def __str__(self):
        out = ""
        if self.parent is None:
            out += f"{self.type} {self.path}"
        else:
            out += f"{self.type} {self.parent.path} {self.path} size: {self.get_size()}"
        
        
        for child in self.children.values():
            
            out += "\n" + "\n".join(map(lambda x: ' '*2 + x, str(child).split('\n')))
        return out


def build_tree(cmd_list):
    dirs = []
    root_leaf = Leaf(parent=None, path='/')
    current_leaf = root_leaf
    
    for line in cmd_list:
        if line.startswith('$ cd') and ('..' not in line):
            current_path = line.split(' ')[-1]
            if current_path in current_leaf.children:
                current_leaf = current_leaf.children[current_path]
            else:
                new_leaf = Leaf(current_leaf, current_path)
                current_leaf.children[current_path] = new_leaf
                current_leaf = new_leaf
        elif line.startswith('$ cd') and '..' in line:
            current_leaf = current_leaf.parent
        elif not line.startswith('$') and line.startswith('dir'):
            _, dir_name = line.split(' ')
            current_leaf.children[dir_name] = Leaf(current_leaf, dir_name)
            dirs.append(current_leaf.children[dir_name])
        elif not line.startswith('$'):
            size, file_name = line.split(' ')
            current_leaf.children[file_name] = Leaf(current_leaf, dir_name, type='File', size=int(size))
    
    return root_leaf, dirs
with open(sys.argv[1], 'r') as fin:
    tree, dirs = build_tree(filter(lambda x: x, fin.read().split('\n')))
    print('-'* 50)
    print(tree)
    right_dirs = list(filter(lambda x: x< 100000, map(lambda x: x.get_size(), dirs)))
    print('Answer question 1', right_dirs, reduce(lambda x, y: x + y, right_dirs, 0))

    total_disk_space = 70000000
    required_disk_space = 30000000
    space_left = total_disk_space - tree.get_size()
    to_delete = required_disk_space - space_left
    
    print('Answer question 2', sorted(filter(lambda x: x >= to_delete, [ d.get_size() for d in dirs])))

        
    