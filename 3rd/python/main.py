import sys
from functools import reduce


def type_to_int(type_name:str):
    offset = ord('a') - 1
    high_offset = ord('A') - 27
    if type_name.islower():
        return ord(type_name) - offset
    elif type_name.isupper():
        return ord(type_name) - high_offset
    else:
        raise TypeError(type_name)

def sum_items(a,b):
    return a+b

def find_common_items_and_sum(contents):
    common = set(contents[0])
    for content in contents[1:]:
        common = common.intersection(set(content))
    return reduce(sum_items, map(type_to_int, common), 0)

def split_in_tree(items):
    return items[0::3], items[1::3], items[2::3]

with open(sys.argv[1], 'r') as fin:
    
    all_lines = fin.read().split('\n')
    total = reduce(sum_items,
                   map(lambda line: 
                       find_common_items_and_sum((line[:len(line)//2], line[len(line)//2:])),
                       all_lines)
                  )
    print('Total score of elves rugsack', total)

    total = reduce(sum_items, map(find_common_items_and_sum, zip(*split_in_tree(all_lines))))
    
    print('Total scores of group of elves', total)