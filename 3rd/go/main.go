package main

import (
	"fmt"
	"os"
	"strings"
	"unicode"
)

type StringSet map[string]struct{}

func (a StringSet) has(item string) bool {
	if _, ok := a[item]; ok {
		return true
	} else {
		return false
	}
}

func (a StringSet) add(item string) {
	a[item] = struct{}{}
}

func (a StringSet) addSingle(item string) {
	for _, s := range item {
		a.add(string(s))
	}
}

func (a StringSet) remove(item string) {
	delete(a, item)
}

func (a StringSet) intersect(b StringSet) StringSet {
	result := StringSet{}
	for key := range b {
		if a.has(key) {
			result.add(key)
		}
	}
	return result
}

func (a StringSet) isSubset(b StringSet) bool {
	return len(a.intersect(b)) == len(b)
}

const Ca int = 'a'
const CA int = 'A'

func (a StringSet) countScore() (total int) {
	for character := range a {
		if unicode.IsUpper(rune(character[0])) {
			total += int(int(character[0])-CA) + 27
		} else {
			total += int(int(character[0])-Ca) + 1
		}
	}
	return
}

func main() {
	//defer func() {
	//	if e := recover(); e != nil {
	//		fmt.Println(e)
	//	}
	//}()

	if len(os.Args) != 2 {
		fmt.Println("Please specify an input file")
		panic("parameter missing")
	}
	fname := os.Args[1]

	content, err := os.ReadFile(fname)
	if err != nil {
		panic(err)
	}

	contentLines := strings.Split(string(content), "\n")
	totalScore := 0
	for _, line := range contentLines {

		a, b := StringSet{}, StringSet{}
		stringSize := len(line)
		a.addSingle(line[:stringSize/2])
		b.addSingle(line[stringSize/2:])
		totalScore += a.intersect(b).countScore()
	}

	fmt.Println("Total score of elves rugsack", totalScore)

	totalScore = 0
	for i := 0; i < len(contentLines)/3; i++ {

		a, b, c := StringSet{}, StringSet{}, StringSet{}

		a.addSingle(contentLines[i*3])
		b.addSingle(contentLines[i*3+1])
		c.addSingle(contentLines[i*3+2])

		totalScore += a.intersect(b).intersect(c).countScore()
	}

	fmt.Println("Total score of elves rugsack", totalScore)

}
