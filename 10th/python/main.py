import sys

import numpy as np


class SingleMem:

    def __init__(self, draw=False, interesting_cycles=(20, 60, 100, 140, 180, 220)):
        self.interesting_cycles = interesting_cycles
        self.values = 1
        self.draw = draw
        self.cycle_counter = 0
        self.adder = []
        self.screen = ['' for k in range(6)]
        self.current_line = ['.' for _ in range(40)]

    def is_reached(self):
        if self.cycle_counter in self.interesting_cycles:
            return True
        else:
            return False

    def draw_line(self):
        print(''.join(self.current_line))

    def draw_sprite(self):
        index = self.cycle_counter % 40
        self.current_line[
            index] = '#' if self.values - 1 <= index <= self.values + 1 else '.'

    def tick(self):
        self.draw_sprite()
        self.cycle_counter += 1

        if self.is_reached() and self.draw:
            self.draw_line()

    def do(self, instruction):
        if 'noop' in instruction:
            n_cycles = 1
        else:
            n_cycles = 2

        for i in range(n_cycles):
            self.tick()
            if self.is_reached():
                self.adder.append(self.values * self.cycle_counter)

            if n_cycles == 2 and i == 1:
                inst, value = instruction.split(' ')
                self.values += int(value)


with open(sys.argv[1], 'r') as fin:
    instructions = list(filter(lambda x: x, fin.read().split('\n')))
    mem = SingleMem()
    for instruction in instructions:
        mem.do(instruction)
    print('First')
    print(np.sum(mem.adder[:6]))
    print('Second')
    mem = SingleMem(draw=True, interesting_cycles=(40, 80, 120, 160, 200, 240))
    for instruction in instructions:
        mem.do(instruction)
    print(np.sum(mem.adder[:6]))
