#!/usr/bin python3
import requests
import datetime
import os


STARTTIME = datetime.time(hour=6)
COMPETITION_STARTDATE = datetime.date(year=2022, month=12, day=1)

def unix_time_to_datetime(unix_ts: int):
    return datetime.datetime.fromtimestamp(unix_ts)

def time_spent(day, submission_datetime):
    start_day_datetime = datetime.datetime.combine(date=COMPETITION_STARTDATE + datetime.timedelta(days=day), time=STARTTIME)
    time_interval = submission_datetime - start_day_datetime
    return f'{time_interval.seconds} s', f'{time_interval.seconds / 60} min'


def parse_star(day, data):
    time = unix_time_to_datetime(data['get_star_ts'])
    return {'index': data['star_index'], 
            'time': repr(time),
            'time_spent': time_spent(day, time)}

def parse_day(day, data):
    return {item: parse_star(int(day) ,value) for item, value in data.items()}


class Partecipant:
    def __init__(self, data):
        self.name = data['name']
        self.last_star_obtained_at = unix_time_to_datetime(data['last_star_ts'])
        self.stars = data['stars']
        self.global_score = data['global_score']
        self.local_score = data['local_score']
        self.days = {day : parse_day(day, day_data) for day, day_data in data['completion_day_level'].items()}
        

    def print_summary(self):
        print(f'{self.name} -{self.local_score}/{self.global_score}- {repr(self.last_star_obtained_at)}')

        days = sorted(self.days.keys())
        for day in days:
            day_string = f'-- day {day}'
            print(f'-- day {day}',f'1st star {repr(self.days[day]["1"])}')
            print(' ' * len(day_string),f'2st star {repr(self.days[day]["2"])}')
            

def read_session_cookie():
    session_config_path = os.path.expanduser('~/.config/aoc/session') 
    if os.path.exists(session_config_path):
        with open(session_config_path, 'r') as fin:
            session = fin.read().strip()
            if session:
                return session
            else:
                print('Please put a session string at ', session_config_path)
    else:
        print('Please create session config file at path ', session_config_path)

import logging

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)
URL_SCOREBOARD = 'https://adventofcode.com/2022/leaderboard/private/view/36900.json'

if __name__ == '__main__':
    cookies = {'session': read_session_cookie()}
    response = requests.get(URL_SCOREBOARD, cookies=cookies)
    if response.ok:
        try:
            partecipants = [Partecipant(data) for data in response.json()['members'].values()]
            partecipants = sorted(partecipants, key=lambda x: x.local_score, reverse=True)
            for partecipant in partecipants:
                partecipant.print_summary()
        except Exception as e:
            logging.exception(e)
            logging.error('Your session cookie is invalid or expired please check')