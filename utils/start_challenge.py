import os
import requests
from argparse import ArgumentParser
import datetime


STARTTIME = datetime.time(hour=6)
COMPETITION_STARTDATE = datetime.date(year=2022, month=12, day=1)

def get_current_day():
    day_since_starting = (datetime.date.today() - COMPETITION_STARTDATE).days + 1
    return day_since_starting

def parse_args():
    parser = ArgumentParser(description='Prepare folder for the challenge of the day')
    parser.add_argument('--day', help='Specify the day number', default=get_current_day(), type=int)
    parser.add_argument('directory', help='Directory where to store the files')
    return parser.parse_args()

def read_session_cookie():
    session_config_path = os.path.expanduser('~/.config/aoc/session') 
    if os.path.exists(session_config_path):
        with open(session_config_path, 'r') as fin:
            session = fin.read().strip()
            if session:
                return session
            else:
                print('Please put a session string at ', session_config_path)
    else:
        print('Please create session config file at path ', session_config_path)

import logging

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)
URL_SCOREBOARD = 'https://adventofcode.com/2022/day/{:}/input'


if __name__ == '__main__':
    args = parse_args()
    cookies = {'session': read_session_cookie()}
    print(URL_SCOREBOARD.format(args.day))
    inputs_file = requests.get(URL_SCOREBOARD.format(args.day), cookies=cookies).content
    os.makedirs(args.directory, exist_ok=True)
    data_path = os.path.join(args.directory, 'data')
    os.makedirs(data_path, exist_ok=True)
    with open(os.path.join(data_path, 'input'), 'wb') as fout:
        fout.write(inputs_file)
    
    