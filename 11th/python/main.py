import sys

def functor(operation):
    def func(old):
        ldict = {'old': old}
        exec(operation, globals(), ldict)
        return ldict['new']
    return func


class Monkey:
    def __init__(self, story, boredom_factor=3):
        self.monkey_id = -1
        self.stash = []
        self.transform = lambda x: x
        self.test_prime = 1
        self.true_monkey = -1
        self.false_monkey = -1
        self.boredom_factor = boredom_factor
        self.parse_story(story)
        self.inspect_count = 0
        self.ring_size = -1

    def parse_story(self, text):
        lines = text.split('\n')
        for line in lines:
            if line.startswith('Monkey'):
                self.monkey_id = int(line.split(' ')[1].strip(':'))
            if 'Starting items: ' in line:
                self.stash = [int(item.strip()) for item in line.split(':')[1].strip().split(',')]
            elif 'Operation:' in line:
                self.transform = functor(line.split(':')[1].strip())
            elif 'Test:' in line:
                self.test_prime = int(line.split('divisible by ')[1])
            elif 'true:' in line:
                self.true_monkey = int(line.split('throw to monkey ')[1].strip())
            elif 'false:' in line:
                self.false_monkey = int(line.split('throw to monkey ')[1].strip())

    def evaluate_single(self, item):
        self.inspect_count += 1
        t_item = self.transform(item)
        if not isinstance(t_item, CleaverNumber):
            t_item = t_item // self.boredom_factor

        if self.ring_size > 0:
            t_item = t_item % self.ring_size
        if t_item % self.test_prime == 0:
            return t_item, self.true_monkey
        else:
            return t_item, self.false_monkey

    def think(self):
        reasoning = []
        for item in self.stash:
            reasoning.append(self.evaluate_single(item))
        self.stash = []
        return reasoning

    def print(self):
        print(f'Monkey: {self.monkey_id}'.center(20, '-'))
        print('Stash: ', self.stash)
        print('Transform: ', 1, '->', self.transform(1))
        print('Test prime: ', self.test_prime)
        print('Monkeys: ', self.true_monkey, self.false_monkey)



if __name__ == '__main__':
    with open(sys.argv[1], 'r') as fin:

        monkeys_test = fin.read().split('\n\n')
        if len(sys.argv) == 2:
            monkeys = [Monkey(monkey) for monkey in monkeys_test]
            for round in range(int(sys.argv[2])):
                for monkey in monkeys:
                    for action in monkey.think():
                        worry_level, monkey_receiving = action
                        monkeys[monkey_receiving].stash.append(worry_level)
                print(f'Round {round + 1}'.center(20, '-'))
                for monkey in monkeys:
                    print(monkey.monkey_id, monkey.stash)

            sorted_monkeys = sorted(monkeys, key=lambda x: x.inspect_count, reverse=True)
            for monkey in sorted_monkeys:
                print('Monkey ', monkey.monkey_id, 'inspected items', monkey.inspect_count, 'times')

            print('business score', sorted_monkeys[0].inspect_count * sorted_monkeys[1].inspect_count)
        #====== NUMBER 2
        monkeys = [Monkey(monkey, boredom_factor=1) for monkey in monkeys_test]
        RING_SIZE = 1
        for monkey in monkeys:
            RING_SIZE *= monkey.test_prime
        for monkey in monkeys:
            monkey.ring_size = RING_SIZE

        for round in range(int(sys.argv[2])):
            for monkey in monkeys:
                for action in monkey.think():
                    worry_level, monkey_receiving = action

                    monkeys[monkey_receiving].stash.append(worry_level)
            print(f'Round {round + 1}'.center(20, '-'))

        sorted_monkeys = sorted(monkeys, key=lambda x: x.inspect_count, reverse=True)
        for monkey in sorted_monkeys:
            print('Monkey ', monkey.monkey_id, 'inspected items', monkey.inspect_count,
                  'times')

        print('business score',
              sorted_monkeys[0].inspect_count * sorted_monkeys[1].inspect_count)

