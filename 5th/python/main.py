import sys

class Stack:

    def __init__(self, start_stack):
        self.stack = [list(column) for column in zip(*[[c for c in line[1::4]] for line in start_stack.split('\n')[:-1]])]
        self.n_columns = len(self.stack)
        self.clean_stacks()

    def do(self, instr):
        _, n_times, _ ,from_stack, _ , to_stack = instr.split(' ')
        for i in range(int(n_times)):
            self.move_single(int(from_stack)-1, int(to_stack)-1)
    def do6001(self, instr):
        _, n_times, _ ,from_stack, _ , to_stack = instr.split(' ')
        self.move_bulk(int(n_times), int(from_stack) -1 , int(to_stack)- 1)

    def clean_stacks(self):
        for stack in self.stack:
            while stack.count(' '):
                stack.remove(' ')

    def move_single(self, from_stack, to_stack):
        k = self.stack[from_stack][0]
        self.stack[to_stack] = [k] + self.stack[to_stack]
        self.stack[from_stack].pop(0)

    def move_bulk(self, n_boxes, from_stack, to_stack):
        boxes = self.stack[from_stack][:n_boxes]
        self.stack[to_stack] = boxes + self.stack[to_stack]
        self.stack[from_stack] = self.stack[from_stack][n_boxes:]

    def first_of_every(self):
        return [col[0] for col in self.stack]

    def n_rows(self):
        n_rows = -1
        for col in self.stack:
            n_rows = max(len(col), n_rows)
        return n_rows

    def __str__(self):
        screen = [" " * 4 * self.n_columns for _ in range(self.n_rows())]
        for k, row in enumerate(self.stack):
            for l, value in enumerate(row[::-1]):
                screen[l] = screen[l][:k*4] + f'[{value}]' + screen[0][k*4+3:]
        return "\n".join(['-' * 4 * self.n_columns] + screen[::-1] + ['-' * 4 * self.n_columns])


with open(sys.argv[1], 'r') as fin:
    start_stack, moves = fin.read().split('\n\n')
    stack = Stack(start_stack)
    print('=== Section 1 ===')
    print('Starting stack')
    print(stack)

    for move in moves.split('\n'):
        if move == '':
            continue
        stack.do(move)
    print('Ending stack')
    print(stack)
    print('Top of the stack is', ''.join(stack.first_of_every()))
    print('=== Section 2 ===')
    stack = Stack(start_stack)
    print('Starting stack')
    print(stack)


    for move in moves.split('\n'):
        if move == '':
            continue
        stack.do6001(move)
    print('Ending stack')
    print(stack)        
    print('Top of the stack is', ''.join(stack.first_of_every()))