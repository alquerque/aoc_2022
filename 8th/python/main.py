import sys
import numpy as np


def grad_top_bottom(grid):
    grad = np.ones_like(grid)
    for j in range(1, grid.shape[1] - 1):
        max_tree = grid[0, j]
        
        for i in range(1, grid.shape[0] - 1 ):
            grad[i, j] = grid[i, j] > max_tree
            max_tree = max(max_tree, grid[i, j])
            
    return grad

def grad_bottom_top(grid):
    grad = np.ones_like(grid)
    for j in range(1, grid.shape[1] - 1):
        max_tree = grid[-1, j]
        for i in range(1, grid.shape[0] - 1):
            grad[-i - 1, j] = grid[-i -1, j] > max_tree
            max_tree = max(max_tree, grid[-i - 1, j])
            
    return grad

def grad_left_right(grid):
    grad = np.ones_like(grid)
    for i in range(1, grid.shape[0] - 1):
        max_tree = grid[i, 0]
        for j in range(1, grid.shape[1] - 1):
            grad[i, j] = grid[i, j] > max_tree
            max_tree = max(max_tree, grid[i, j])
            
    return grad

def grad_right_left(grid):
    grad = np.ones_like(grid)
    for i in range(1, grid.shape[0] - 1):
        max_tree = grid[i, -1]
        for j in range(1, grid.shape[1] - 1):
            grad[i, -j-1] = grid[i, -j-1] > max_tree
            max_tree = max(max_tree, grid[i, -j - 1])
            
    return grad

def score_for_pos_dir(grid, x, y, d):
    x_shape, y_shape = grid.shape
    end_index_x = x + 1 if d[0] > 0 else x_shape - x
    end_index_y = y + 1 if d[1] > 0 else y_shape - y
    end_index = end_index_x if d[0] != 0 else end_index_y
    value = grid[x, y]
    total = 0
    for k in range(1, end_index):
        total += 1
        if grid[x - k * d[0], y - k * d[1]] >= value:
            break
    return total
    

def view_scores_dir(grid, d):
    result = np.zeros_like(grid)
    for i in range(grid.shape[0]):
        for j in range(grid.shape[1]):
            result[i, j] = score_for_pos_dir(grid, i, j, d)
    return result


def read_input_grid(fname):
    with open(fname, 'r') as fin:
        input_grid = np.array([[int(c) for c in line] for line in fin.read().split('\n') if line])
        return input_grid
    
grid = read_input_grid(sys.argv[1])

top_bottom_grad = grad_top_bottom(grid)
bottom_top_grad = grad_bottom_top(grid)
left_right_grad = grad_left_right(grid)
right_left_grad = grad_right_left(grid)

visible = top_bottom_grad + bottom_top_grad > 0
visible = visible + left_right_grad
visible = visible + right_left_grad
visible = visible > 0

print('Result', '-'*20)
print(visible)
print("# of the possible sites", visible.sum())

t = view_scores_dir(grid, (1, 0))
b = view_scores_dir(grid, (-1, 0))
l = view_scores_dir(grid, (0, 1))
r = view_scores_dir(grid, (0, -1))

scores = t * b * l * r

print('Result2', '-'*20)
print(scores)
print("Max view score", scores.max())
    

    
