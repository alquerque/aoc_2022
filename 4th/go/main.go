package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Set[T comparable] struct {
	keys []T
}

func (a *Set[T]) has(item T) bool {
	for _, value := range a.keys {
		if value == item {
			return true
		}
	}
	return false
}

func (a *Set[T]) add(item T) {
	if !a.has(item) {
		a.keys = append(a.keys, item)
	}
}

func (a *Set[T]) intersect(b Set[T]) Set[T] {
	result := Set[T]{}
	for _, key := range b.keys {
		if a.has(key) {
			result.add(key)
		}
	}
	return result
}

func (a *Set[T]) isSubset(b Set[T]) bool {
	return len(a.intersect(b).keys) == len(b.keys)
}

func rangeStringToSet(ran string) Set[int] {
	boundaries := strings.Split(ran, "-")
	from, _ := strconv.Atoi(boundaries[0])
	to, _ := strconv.Atoi(boundaries[1])
	return rangeToSet(from, to)
}
func rangeToSet(from, to int) (result Set[int]) {
	for i := from; i <= to; i++ {
		result.add(i)
	}
	return
}

func main() {
	if len(os.Args) != 2 {
		panic("Please specify an input file")
	}

	fname := os.Args[1]
	data, err := os.ReadFile(fname)
	if err != nil {
		panic("Cannot read file")
	}

	total := 0
	partly := 0
	lines := strings.Split(string(data), "\n")
	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		ranges := strings.Split(line, ",")

		first := rangeStringToSet(ranges[0])
		second := rangeStringToSet(ranges[1])
		if first.isSubset(second) || second.isSubset(first) {
			total += 1
		}
		if len(first.intersect(second).keys) > 0 {
			partly += 1
		}

	}

	fmt.Println("Totally contained sets are: ", total)
	fmt.Println("Partly contained sets are: ", partly)

}
