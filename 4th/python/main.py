import sys

def range_to_set(range_str):
    start,end = map(int, range_str.split('-'))
    result={i for i in range(start, end + 1)}
    print(start, end, result) 
    return result

with open(sys.argv[1], 'r') as fin:

    data = filter(lambda x: x, fin.read().split('\n'))
    
    total = 0
    partly = 0
    for line in data:
        f, s = map(range_to_set, line.split(','))
        if f.issubset(s) or s.issubset(f):
            total += 1
        if f.intersection(s):
            partly += 1
print(total, partly)