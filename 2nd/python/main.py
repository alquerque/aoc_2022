
"""
A Rock         X 1 LOSE
B Paper        Y 2 DRAW
C Scissor      Z 3 WIN
"""
STRING_TO_SCORE_GUESSED = {
    "A X": 3 + 1,
    "A Y": 6 + 2,
    "A Z": 0 + 3,
    "B X": 0 + 1,
    "B Y": 3 + 2,
    "B Z": 6 + 3,
    "C X": 6 + 1,
    "C Y": 0 + 2,
    "C Z": 3 + 3
}

STRING_TO_SCORE_ELF = {
    "A X": 0 + 3,
    "A Y": 3 + 1,
    "A Z": 6 + 2,
    "B X": 0 + 1,
    "B Y": 3 + 2,
    "B Z": 6 + 3,
    "C X": 0 + 2,
    "C Y": 3 + 3,
    "C Z": 6 + 1
}

import sys
from functools import reduce

def apply_rules_to_strategy(fname, rules):
    with open(fname, 'r') as fin:
        return reduce(lambda x, y: x+y, map(lambda x: rules[x], filter(lambda x: x, fin.read().split('\n'))))

print('The score with the rules you guessed is', apply_rules_to_strategy(sys.argv[1], STRING_TO_SCORE_GUESSED))
print('The score with the rules you guessed is', apply_rules_to_strategy(sys.argv[1], STRING_TO_SCORE_ELF))

