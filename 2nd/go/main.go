package main

import (
	"fmt"
	"os"
	"strings"
)

func applyStrategy(strategy []string, rules map[string]int) int {
	counter := 0
	for _, turn := range strategy {
		if turn != "" {
			counter += rules[turn]
		}
	}
	return counter
}

func main() {
	rulesGuessed := map[string]int{
		"A X": 3 + 1,
		"A Y": 6 + 2,
		"A Z": 0 + 3,
		"B X": 0 + 1,
		"B Y": 3 + 2,
		"B Z": 6 + 3,
		"C X": 6 + 1,
		"C Y": 0 + 2,
		"C Z": 3 + 3,
	}
	rulesElf := map[string]int{
		"A X": 0 + 3,
		"A Y": 3 + 1,
		"A Z": 6 + 2,
		"B X": 0 + 1,
		"B Y": 3 + 2,
		"B Z": 6 + 3,
		"C X": 0 + 2,
		"C Y": 3 + 3,
		"C Z": 6 + 1,
	}

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Error: ", r)
		}
	}()

	fname := ""

	if len(os.Args) == 2 {
		fname = os.Args[1]
	} else {
		panic("please specify a file path")
	}

	data, err := os.ReadFile(fname)
	if err != nil {
		panic("cannot read file :" + fname)
	}

	strategy := strings.Split(string(data), "\n")
	fmt.Println(
		"The score with the guessed rules is",
		applyStrategy(strategy, rulesGuessed),
	)

	fmt.Println(
		"The score with the elf rules is",
		applyStrategy(strategy, rulesElf),
	)

}
