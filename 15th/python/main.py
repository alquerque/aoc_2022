import sys
import re
import tqdm
import math
def parse_sensor_line(string):
    sensor, beacon = re.findall('(x=(-{0,1}\d*),\sy=(-{0,1}\d*))', string)
    sensor_pos = tuple(map(int, sensor[1:]))
    beacon_pos = tuple(map(int, beacon[1:]))
    return sensor_pos, beacon_pos

def find_boundaries(grid):
    min_x, max_x = 0, 0
    min_y, max_y = 0, 0
    for key, value in grid.items():
        min_x = min([key[0], value[0], min_x])
        min_y = min([key[1], value[1], min_y])
        max_x = max([key[0], value[0], max_x])
        max_y = max([key[1], value[1], max_y])
    return [min_x, max_x, min_y, max_y]


def draw_sensor_array(grid):
    min_x, max_x, min_y, max_y = find_boundaries(grid)
    beacon_set = set(grid.values())
    sensor_set = set(grid.keys())

    def pos_to_char(pos):

        if pos in beacon_set:
            return 'B'
        if pos in sensor_set:
            return 'S'
        return '.'
    for j in range(min_y, max_y + 1):
        print(''.join([pos_to_char((i, j)) for i in range(min_x, max_x + 1)]))

def distance(a, b):
    return abs(b[1] - a[1]) + abs(b[0] - a[0])

def compute_sensitivity_radius(grid):
    sensitivity = {}
    for key, value in grid.items():
        sensitivity[key] = distance(value, key)
    return sensitivity

def coverage_at_row(sensor_array, row_id, sensitivity=None):
    coverage = set()
    if sensitivity is None:
        sensitivity = compute_sensitivity_radius(sensor_array)
    beacon_set = set(sensor_array.values())
    for sensor, radius in sensitivity.items():
        v_distance = abs(sensor[1] - row_id)
        if v_distance > radius:
            continue
        else:
            proj_x = int(radius - v_distance)
            for k in range(-proj_x + sensor[0], proj_x + 1 + sensor[0], 1):
                if (k, row_id) not in beacon_set and distance((k, row_id), sensor) <= radius:
                    coverage.add((k, row_id))
    return coverage

class Range:
    def __init__(self, a, b):
        self.start, self.end = a, b

    def is_joint(self, range):
        if self.contains(range.start) or self.contains(range.end):
            return True
        else:
            return False

    def join(self, range):
        self.start = min([self.start, range.start])
        self.end = max([self.end, range.end])

    def contains(self, x):
        if self.start <= x <= self.end:
            return True
        else:
            return False

    @classmethod
    def add_to_coverage(cls, coverage: "List[Range]", range):

        found: "Union[Range, None]" = None
        for i, r in enumerate(coverage):
            if r.is_joint(range) or range.is_joint(r):
                found = coverage.pop(i)
                break
        if found is None:
            coverage.append(range)
            return
        else:
            found.join(range)
            for i, rr in enumerate(coverage):
                if rr.is_joint(found) or found.is_joint(rr):
                    rr.join(found)
                    return
            else:
                coverage.append(found)

    def size(self):
        return self.end - max(self.start, 0)

    def __str__(self):
        return f'Range({self.start}, {self.end})'
    def __repr__(self):
        return self.__str__()

def coverage_at_row_ranges(sensor_array, row_id, sensitivity=None):
    coverage = []
    if sensitivity is None:
        sensitivity = compute_sensitivity_radius(sensor_array)

    for sensor, radius in sensitivity.items():
        v_distance = abs(sensor[1] - row_id)
        if v_distance > radius:
            continue
        else:
            proj_x = int(radius - v_distance)
            range = Range(-proj_x + sensor[0], proj_x  + sensor[0])
            Range.add_to_coverage(coverage, range)

    return coverage


def search_with_limit(sensor_array, limit):
    beacon_set = set(sensor_array.values())
    sensitivity = compute_sensitivity_radius(sensor_array)
    for row_id in tqdm.tqdm(range(0, limit + 1)):
        coverage = coverage_at_row_ranges(sensor_array, row_id, sensitivity)
        found = False
        for k in coverage:
            if k.size() >= limit:
                found = True
                break
        if found:
            continue
        print('NOt found', coverage)
        for k in range(0, limit + 1):
            cell = (k, row_id)
            for item in coverage:
                if item.contains(k) or cell in beacon_set:
                    break
            else:
                return cell

if __name__ == '__main__':
    with open(sys.argv[1], 'r') as fin:
        sensors_data = list(filter(lambda x: x, fin.read().split('\n')))
        sensor_array = {}
        for sensor_data in sensors_data:
            sensor_pos, beacon_pos = parse_sensor_line(sensor_data)
            sensor_array[sensor_pos] = beacon_pos

        sensitivity = compute_sensitivity_radius(sensor_array)

        if len(sys.argv) > 5:
            draw_sensor_array(sensor_array)
        selected_row = int(sys.argv[2])
        print(f'Coverage at row {selected_row}', len(coverage_at_row(sensor_array, selected_row)))
        limit = int(sys.argv[3])
        print('Found missing beacon at', search_with_limit(sensor_array, limit))